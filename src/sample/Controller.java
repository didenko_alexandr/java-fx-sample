package sample;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private PasswordField pass;

    @FXML
    private TextField log;

    @FXML
    private Button in;

    @FXML
    private Button reg;

    @FXML
    void initialize() {
        in.setOnAction(event -> {
            System.out.println("Вы вошли!");
        });

    }
}

