package sample;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class SignUpController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField country;

    @FXML
    private Button in;

    @FXML
    private PasswordField password;

    @FXML
    private TextField surname;

    @FXML
    private PasswordField login;

    @FXML
    private TextField name;

    @FXML
    private CheckBox male;

    @FXML
    private CheckBox female;

    @FXML
    void initialize() {

    }
}

